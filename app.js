var express = require('express')
var path = require('path');
var mongoose = require('mongoose');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());


mongoose.connect('mongodb://localhost/react');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

var tweetSchema = mongoose.Schema({
  author: {type: String, default: 'Anonymous'},
  tweet_date: {type: Date, required: false},
  tweet_text: {type: String}
});

var Tweet = mongoose.model('Tweet', tweetSchema);

app.post('/api/comments', function(req, res) {
  console.log('post data', req.body.tweet_text);
  var tweet = new Tweet({
    tweet_text: req.body.tweet_text,
    tweet_date: new Date()
  });
  console.log('data to be sent', tweet);
  tweet.save(function(err) {
    if (err) {
      console.error("Error while saving: ", err);
    } else {
      console.log('saved');
    }
  });
});

app.get('/api/tweets', function(req, res){
  console.log("I received a GET request");
  Tweet.find({}, function (err, tweets) {
    res.send(tweets);
    console.log('response',tweets);
  }).sort({tweet_date: -1});;
});

app.get('/',function(req,res){
  res.sendFile(path.join(__dirname, 'index.html'));
}),

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})


